The Life Sciences (LS) Product 360 package is an application package that consists of predefined Life Sciences Product components.  This package is a software solution to manage medicinal products, their components, manufacturers, regulatory bodies, and other aspects of the pharmaceutical product industry.

##IDMP Standards
This solution closely adheres to the ***IDMP standards*** and is compatible with the HL7 standard for inbound and outbound use cases. 

This repository lists all of the documents and code that is being developed as part of the Product 360 build out.  
This is designed to be extensible but also a complete solution for intended to provided a fully function application "out of the box".


## Change Log

```
Last Update Date: 1/08/2018
Version: 2018.DDA.8
Description:
Updated RDM Config from the last release - 2018.DDA.8
```

## Contributing
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/master/CodeOfConduct.md) to learn more about our contribution guidlines

## Licensing
```
Copyright (c) 2018 Reltio   
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start
To learn about LS Product 360 data model view these [documents](The Life Sciences (LS) Product 360 package is an application package that consists of predefined Life Sciences Product components.  This package is a software solution to manage medicinal products, their components, manufacturers, regulatory bodies, and other aspects of the pharmaceutical product industry.

### Repository Owners
[Naveen Gupta](naveen.gupta@reltio.com)
[Gregg Casey](gregg.casey@reltio.com)

[Yuriy Raskin](yuriy.raskin@reltio.com)